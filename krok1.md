# Logujemy na zeusa przez SSH lub PuTTY
    ssh username@ui.cyfronet.pl

 jeżeli nam każe to wpisujemy hasło
# Kopiujemy case'a ze strony /~lojek/student.html i rozpakowujemy
    wget http://home.agh.edu.pl/~lojek/of_files/radialFan.tar.gz
    tar -zxvf radialFan.tar.gz
# Zmieniamy predkosc na zadaną
    cd radialFan
    cd 0
    nano U

tutaj szukamy frazy

    Inlet
        {
        type surfaceNormalFixedValue;
            refValue uniform -8; // ujemna bo wlot w m/s

i na przyklad jeżeli mamy zadaną prędkość 9.5m/s, to zmieniamy z `refValue -8;` na `refValue -9.5;` i zapisujemy wciskając CTRL+X, Y, Enter
# Zmieniamy nazwę case'a by nie było potem 15 caseów z takimi samymi nazwami
    cd ~

na przykład jeżeli zadana prędkość to 9.5 m/s to:

    mv radialFan radialFan95
# Kopiujemy case'a do katalogu $SCRATCH
    cp radialFan95 $SCRATCH -r

# Sprawdzamy czy się skopiowało
    cd $SCRATCH
    ls

jeżeli pokaże się folder radialFanXX/ to jest ok
# Ściągamy skrypt do kolejkowania
    cd ~
    wget http://home.agh.edu.pl/~lojek/of_files/skrypt

# Sprawdzamy nazwę swojego grantu
    zeus-show-grants
pokaże się coś takiego:

    Your active PL-Grid grants on THIS site:
    +--------------+------------+------------+--------------------+-------------------+--------------------+-------------------+-----------+
    | GrantID      | Start Date |  End Date  | Total Walltime [h] | Used Walltime [h] | Total Storage [GB] | Used Storage [GB] |   Group   |
    +--------------+------------+------------+--------------------+-------------------+--------------------+-------------------+-----------+
    | plgjd692019a | 2019-10-30 | 2020-10-30 |              1 000 |                 0 |                 10 |                 0 | plgt-jd69 |
    +--------------+------------+------------+--------------------+-------------------+--------------------+-------------------+-----------+
    Could not check your default grant ID.

Patrzymy jak nazywa się nasze grantID (w moim przypadku plgjd692019a) i je zapamiętujemy lub zapisujemy na karteczce (uwaga na małe/duże litery)
# Edytujemy skrypt
    nano skrypt

i na początku nagłówek:

    #!/bin/bash -l
    ##### Nazwa kolejki
    #SBATCH -p plgrid
    ##### Ilosc wezlow
    #SBATCH -N 1
    ##### Liczba rdzeni
    #SBATCH --ntasks-per-node=2
    ##### Maksymalna pamiec przydzielona jeden proces zadania
    #SBATCH --mem-per-cpu=5GB
    ##### Maksymalny czas obliczen 1 godzina
    #SBATCH --time=30:00:00
    ##### Nazwa zadania widoczna w systemie kolejkowym
    #SBATCH -J radial
    ##### Nazwa grantu uzytego do obliczen
    #SBATCH -A plgjd692019a
    #SBATCH --output="radial.out"
    #SBATCH --error="radial.err"
    ##### ponizej wpisz swoj skrypt
    module add plgrid/apps/openfoam/4.1
    source $OPENFOAM/etc/bashrc
    cd $SCRATCH
    cd radialFan85
    decomposePar
    mpirun -n 2 pimpleDyMFoam -parallel

nazwę kolejki, ilość węzłów, liczbę rdzeni, pamięć, czas obliczeń ustawiamy jak powyżej, zmieniamy nazwę grantu na nazwę zapamiętaną/zapisaną krok wcześniej.
w komendzie `cd axialFan85` zmieniamy 85 na naszą zadaną prędkość - np. dla 8.7m/s będzie `cd axialFan87`
# nadajemy uprawnienia x i odpalamy symulację
    chmod 777 skrypt
    sbatch skrypt
I czekamy na zakonczenie obliczen
status zadania można sprawdzić
    zeus-jobs
