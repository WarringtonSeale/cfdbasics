# Upewniamy się że obliczenia z kroku1 zostały zakończone
    zeus-jobs
Nie powinno się nic wyświetlić

    zeus-jobs-history

Powinny wyświetlić się zlecone obliczenia - sprawdzamy czy Walltime jest równy zadanemu w poprzednim kroku

# Upewniamy się, że obliczenia z korku1 zostały przeprowadzone bez dziwnego errora
    cd ~
    cat radial.err
Przykładowy radial.err świadczący o poprawnym przebiegu obliczeń:

    plgrid/tools/gcc/4.8.2 loaded.
    plgrid/libs/qt/4.8.5 loaded.
    plgrid/tools/gcc/4.8.2 unloaded.
    plgrid/tools/gcc/4.9.2 loaded.
    plgrid/apps/cuda/7.5 loaded.
    plgrid/tools/openmpi/1.10.0-gnu-4.9.2-ib loaded.
    !!!! WARNING !!!!
    !!!! To run OpenFOAM please run now command below: !!!!
    source $OPENFOAM/etc/bashrc
    plgrid/apps/openfoam/4.1 loaded.
    slurmstepd: error: Exceeded step memory limit at some point.

# Edytujemy skrypt

    cd ~
    nano skryptpostprocess
i wklejamy:

    #SBATCH -N 1
    ##### Liczba rdzeni
    #SBATCH --ntasks-per-node=1
    ##### Maksymalna pamiec przydzielona jeden proces     zadania
    #SBATCH --mem-per-cpu=2GB
    ##### Maksymalny czas obliczen 1 godzina
    #SBATCH --time=1:00:00
    ##### Nazwa zadania widoczna w systemie kolejkowym
    #SBATCH -J postpro
    ##### Nazwa grantu uzytego do obliczen
    #SBATCH -A plgjd692019a
    #SBATCH --output="postpro.out"
    #SBATCH --error="postpro.err"
    ##### ponizej wpisz swoj skrypt
    module add plgrid/apps/openfoam/4.1
    source $OPENFOAM/etc/bashrc
    cd $SCRATCH
    cd radialFan85
    
    reconstructPar
    postProcess -func 'patchAverage(name=Inlet, p)'

W skrypcie zmienić należy nazwę folderu w komendzie `cd radialFan85` na nazwę taką jak nadana w poprzednim kroku, nazwę grantu `##### Nazwa grantu uzytego do obliczen    #SBATCH -A plgjd692019a` na nazwę grantu taką, jak wykorzystana w kroku1.

# Odpalamy skrypt
    sbatch skryptpostprocess
Obliczenia trwaja około 5-10 minut
