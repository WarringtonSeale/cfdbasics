# Krok1
Postawienie i rozpoczęcie obliczeń (5 min klikania, 30h obliczeń)
# Krok2
Postprocess wyników (5 min klikania, 5-10 min obliczeń)
# Krok3
Spakowanie i wysłanie wyników (~3 minuty)

# Status przesyłania wyników

| done? | ID | predkosc | wyslane | shellProcessed | pyProcessed |
| --- | --- | --- | ---| ---| ---|
| True | Jan | 8.5 | 16.12.2019 | True | True |
| True | Ela | 7.5 | 18.01.2020 | True | True |
| True | Joanna | 9.0 | 20.01.2020 | True | True |
| True | MaciejD | 8.0 | 16.12.2019 | True | True |
|  | MaciejG | ? |  |  | |
| True | pan blockMesh | 9.5 | Na ostatnią chwile oczywiście | True | True |
| True | Bartosz | 10 | 09.01.2020 | True | True |
| True  | Wojciech | 7 | 08.01.2020 | True | True |
|  | Marcin | ? |  |  | |



# Obecny wygląd wykresu

![](results/Efficiencies.svg)

![](results/pressureAndTorque70.svg)
![](results/pressureAndTorque75.svg)
![](results/pressureAndTorque80.svg)
![](results/pressureAndTorque85.svg)
![](results/pressureAndTorque90.svg)
![](results/pressureAndTorque100.svg)




