# Upewniamy się że obliczenia z kroku2 zostały zakończone
    zeus-jobs

Nie powinno się nic wyświetlić

    zeus-jobs-history

Powinny wyświetlić się zlecone obliczenia - sprawdzamy czy zadanie z kroku jest zakończone

# Wchodzimy do folderu z wynikami

    cd $SCRATCH
    cd radialFan85
    cd postProcessing
    ls

Powinniśmy zobaczyć dwa foldery - jeden odpowiedzialny za siłę, drugi za ciśnienie
Pamiętamy oczywiście o zmianie liczby odpowiedzialnej za predkosc - dla predkosci 7.3m/s komenda bedzie `cd radialFan73`
# Pakujemy oba foldery

    tar -zcvf radial85.tar.gz forces/ patchAverage(name=Inlet,p)/
    ls

Tutaj także pamiętamy o zmianie liczby prędkości w nazwie
Skomplikowane nazwy plików możemy autouzupełniać wpisując dwie-trzy pierwsze litery i wciskając tab.

# Ściągamy utworzonego tara

DO WPISANIA NIE W PUTTY, ALE W TERMINALU(linux/mac) LUB POWERSHELLU(win)


    scp plgjd69@ui.cyfronet.pl:/mnt/lustre/scratch2/people/plgjd69/radialFan85/postProcessing/radial85.tar.gz .

Pamiętamy o zmienieniu:

- `plgjd69` na swój login plgrida
- `radialFan85` na radialFan z odpowiednią liczbą prędkości
- `radialFan85.tar.gz` na radialFan z odpowiednią liczbą prędkości

# Wysyłamy spakowane wyniki do osoby odpowiedzialnej za wykresy

Wyniki przyjmuję w postaci:

- linka do google drive z wrzuconym tarem
- wyslania tara na maila dlugosz@student.agh.edu.pl
- linka do repo na jakims gicie, lub fork/branch i pull request do tego repo
